program Sph;
// https://gitlab.com/Lithe7/sph.git

// ���������  ���������  �����������  �����  �  �����������  .JPG
// ������� YYYY.MM.DD HH-NN-SS*.JPG �� ��������� ����: YYYY_MM_DD

{$APPTYPE CONSOLE}

//{$R *.res}

uses
 Windows;

var
 i, j, fl, FilesCount: integer;
 CurrentDir, FileName, Path: string;
 FindHandle: THandle;
 FindData: TWin32FindData;
 EndPause, Help: boolean;

begin
 EndPause := false;
 Help := false;
 Writeln('��������� ���������� ����������'#13#10' by Lithe ������ 1.1'#13#10);
 for i := 1 to ParamCount do
  begin
   // Help
   if (ParamStr(i) = '/?') or (ParamStr(i) = '-?') or (ParamStr(i) = '?') then
    begin
     for j := 1 to 62 do Write('-'); Writeln;
     Writeln('���������  ���������  �����������  �����  �  �����������  .JPG');
     Writeln('������� YYYY.MM.DD HH-NN-SS*.JPG �� ��������� ����: YYYY_MM_DD');
     for j := 1 to 62 do Write('-'); Writeln;
     Writeln('���������:');
     Writeln('   /? - �������');
     Writeln('   /p - ����� ����� �������');
     Writeln;
     Help := true;
    end;
   // ����� � �����
   if (ParamStr(i) = '/p') or (ParamStr(i) = '-p') or (ParamStr(i) = 'p') then
    EndPause := true;
  end;

 if not Help then
  begin
   FilesCount := 0;
   // �������� � ������� ������� �����
   GetDir(0, CurrentDir);
   Writeln(CurrentDir); for i := 1 to Length(CurrentDir) do Write('-'); Writeln;

   // ������������� �������� �����
   FindData.dwFileAttributes := FILE_ATTRIBUTE_NORMAL;
   // ���� ������ ���� � ������� ��������
   FindHandle := FindFirstFile('*.*', FindData);
   if FindHandle <> INVALID_HANDLE_VALUE then
    repeat
     FileName := FindData.cFileName;
     fl := Length(FileName);
     if (FileName <> '..') and (FileName <> '.') and (fl >= 23) and
        (FileName[1]  in ['1'..'2']) and // ���
        (FileName[2]  in ['0'..'9']) and // ���
        (FileName[3]  in ['0'..'9']) and // ���
        (FileName[4]  in ['0'..'9']) and // ���
        (FileName[5]  = '.') and
        (FileName[6]  in ['0'..'1']) and // �����
        (FileName[7]  in ['0'..'9']) and // �����
        (FileName[8]  = '.') and
        (FileName[9]  in ['0'..'3']) and // �����
        (FileName[10] in ['0'..'9']) and // �����
        (FileName[11] = ' ') and
        (FileName[12] in ['0'..'9']) and // ����
        (FileName[13] in ['0'..'9']) and // ����
        (FileName[14] = '-') and
        (FileName[15] in ['0'..'9']) and // ������
        (FileName[16] in ['0'..'9']) and // ������
        (FileName[17] = '-') and
        (FileName[18] in ['0'..'9']) and // �������
        (FileName[19] in ['0'..'9']) and // �������
        (FileName[20] in [' ','_','.']) and
        (FileName[fl - 3] = '.') and
        (FileName[fl - 2] in ['j','J']) and  // ����������
        (FileName[fl - 1] in ['p','P']) and  // ����������
        (FileName[fl - 0] in ['g','G']) then // ����������
      begin
       Path := FileName[1] + FileName[2] + FileName[3] + FileName[4] + '_' + FileName[6] + FileName[7] + '_' + FileName[9] + FileName[10];
       if CreateDirectory(PChar(Path), NIL) then
        Writeln(' dir created: ' + Path)
       else
        Writeln(' dir exist: ' + Path);
       if MoveFile(PChar(CurrentDir + '\' + FileName), PChar(CurrentDir + '\' + Path + '\' + FileName)) then
        Writeln('  file moved: ' + FileName)
       else
        Writeln('file moving error: ' + FileName);
       FilesCount := FilesCount + 1;
      end;
    until not FindNextFile(FindHandle, FindData);
   FindClose(FindHandle);
   // �������� �����
   if FilesCount > 0 then
    begin
     j := 34;
     if FilesCount > 9999 then
      j := 38
     else
      if FilesCount > 999 then
       j := 37
      else
       if FilesCount > 99 then
        j := 36
       else
        if FilesCount > 9 then
         j := 35;
     for i := 1 to j do Write('-'); Writeln;
     Writeln('������� � ����������: ', FilesCount, ' ����(�,��)')
    end
   else
    Writeln('���������� ����� �� �������!');
  end;
 // ����� ���� ��������
 if EndPause then
  Readln;
end.
